#include <Arduino.h>
#include <OneButton.h>
#include <BleKeyboard.h>

#define PIN_INPUT1 15
#define PIN_INPUT2 13

OneButton button1(PIN_INPUT1, true);
OneButton button2(PIN_INPUT2, true);

BleKeyboard bleKeyboard("Lemona vežė neatvežė", "Lauris Co.", 100);

void click1() {
  bleKeyboard.write(KEY_MEDIA_PREVIOUS_TRACK);
}



void doubleclick1() {
  Serial.println("Button 1 doubleclick.");
  bleKeyboard.write(KEY_MEDIA_PLAY_PAUSE);
}



void longPressStart1() {
  Serial.println("Button 1 longPress start");
}



void longPress1() {
  bleKeyboard.write(KEY_MEDIA_VOLUME_DOWN);
}



void longPressStop1() {
  Serial.println("Button 1 longPress stop");
}

void click2() {
  bleKeyboard.write(KEY_MEDIA_NEXT_TRACK);
}


void doubleclick2() {
  bleKeyboard.write(KEY_MEDIA_PLAY_PAUSE);
}


void longPressStart2() {
  Serial.println("Button 2 longPress start");
}


void longPress2() {
  bleKeyboard.write(KEY_MEDIA_VOLUME_UP);
}

void longPressStop2() {
  Serial.println("Button 2 longPress stop");
}

void setup() {

  bleKeyboard.begin();

  button1.attachClick(click1);
  button1.attachDoubleClick(doubleclick1);
  button1.attachLongPressStart(longPressStart1);
  button1.attachLongPressStop(longPressStop1);
  button1.attachDuringLongPress(longPress1);

  button2.attachClick(click2);
  button2.attachDoubleClick(doubleclick2);
  button2.attachLongPressStart(longPressStart2);
  button2.attachLongPressStop(longPressStop2);
  button2.attachDuringLongPress(longPress2);

  button2.setClickMs(100);
  button1.setLongPressIntervalMs(500);
  button2.setLongPressIntervalMs(500);

}

void loop() {
  button1.tick();
  button2.tick();
  delay(10);
} 


